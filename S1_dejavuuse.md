# Fonctionnalité 2 : Reconnaissance de fichiers audio sur disque

Nous allons maintenant commencer à écrire notre application en créant les différentes fonctionnalités.

Avant toute chose, il faut reflechir à la structuration de votre projet.
Ici, nous allons choisir de créer un package `audio_recognize` dans lequel vous ajouterez  un module permettant de reconnaître, à l'aide de la bibliothèque `dejavu` un fichier audio se trouvant sur disque.


Une fois cet objectif atteint, nous pourrons l'utiliser dans la  [**Fonctionnalité 3** : Prise en compte du micro.](./S1_micro.md)


